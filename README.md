# HyperPubee

HyperPubee is a data structure for written text
which allows (self-)publishing on the decentralised internet.
An interesting feature is the ability to embed parts of other texts.

It currently supports basic poetry and prose texts, as well as collections.

Warning: this library is still in early alfa, and there are likely to be breaking changes before it reaches v1.0.0.

## Install

`npm add hyperpubee`

## Data Structure
HyperPubees are stored in a distributed key-value store called [hyperbee](https://github.com/hypercore-protocol/hyperbee).
Hyperbees allow sparse loading of their content,
which is particularly important if you are using embeddings
(if you are embedding a single line from a book, it would not be very
efficient to first load the entire book to then extract the embedded phrase).

Hyperbees are also versioned, which is important when embedding other texts
(when you embed a specific version, the embedded text will always remain the same,
even if the referenced text is updated).

Hyperbees are not stored in some centralised location, they are stored on a decentralised network.
This implies that you do not have to worry about the work you referenced suddenly becoming unavailable--you can actually help host it. 

HyperPubees are structured in a tree format.
There are currently three kinds of leaf nodes: plain text, embeddings and links.
Non-leaf nodes impose structure
(for example, multiple embeddings can be combined in a line,
multiple lines can be combined in a verse...)

You can create hyperpubees with any kind of self-defined structures you wish,
you will simply have to make sure that the library which renders
them knows how to interpret your structures.
For example, a simple renderer for a poem would introduce a new line after
each 'line' structure, and two new lines after each 'verse' structure.

## Rendering
HyperPubees are meant to be rendered by some front-end library,
such as [hyperpubee-app](https://gitlab.com/HDegroote/hyperpubee-app).

The basic algorithm is:

- Start by requesting the root

- Then Interpret its contents and recursively request all children until reaching leaf nodes

  - When you encounter an embedding, you should request the embedded content
   from the referenced HyperPubee, apply the patch if it exists,
   then render the text.
  - When you encounter a link, you should describe its content
  and provide a way to access the linked work
  (for example: loading and displaying its title,
  and providing a way to load the hyperpubee corresponding to its hash)

## Usage
Note: these examples use the random-access-memory package, so it will need to be installed.

### Poetry
```
const ram = require('random-access-memory')
const HyperInterface = require('hyperpubee-hyper-interface')
const { api, helpers } = require('hyperpubee')
const Corestore = require('corestore')

const corestore = new Corestore(ram)
const hyperInterface = new HyperInterface(corestore)
await hyperInterface.ready()

// Creating a poem
const text = `[poetry]
# My poem

This is an example poem
It is very brief
`

pubee = await api.buildWorkFromText(
  text,
  'my test poem',
  hyperInterface
)

console.log("Created poem with key", pubee.key)

// Normally you would use some front-end renderer,
// but these are not included in this package
// so we print a JSON representation of the internal structure
const resolvedPubee = await helpers.resolvePubeeToJson(pubee)
console.log(JSON.stringify(resolvedPubee, null, 1))

/* Expected output:
[
 {
  "title": [
   "My poem"
  ]
 },
 {
  "poem": [
   {
    "verse": [
     {
      "line": [
       "This is an example poem"
      ]
     },
     {
      "line": [
       "It is very brief"
      ]
     }
    ]
   }
  ]
 }
]
*/

```

### Prose
You can create prose works in the same way, except that you should now indicate the 'prose' type at the start of the text.

```
const ram = require('random-access-memory')
const HyperInterface = require('hyperpubee-hyper-interface')
const { api, helpers } = require('hyperpubee')
const Corestore = require('corestore')

const corestore = new Corestore(ram)
const hyperInterface = new HyperInterface(corestore)
await hyperInterface.ready()

// Creating prose
const text = `[prose]
# My Story

## Chapter 1

This is my story.

I separate paragraphs with 2 newlines.

## Chapter 2

The end!
`

pubee = await api.buildWorkFromText(
  text,
  'my piece of prose',
  hyperInterface
)

console.log("Created prose with key", pubee.key)

const resolvedPubee = await helpers.resolvePubeeToJson(pubee)
console.log(JSON.stringify(resolvedPubee, null, 1))

/* Expected output:
[
 {
  "title": [
   "My Story"
  ]
 },
 {
  "prose": [
   {
    "chapter": [
     {
      "chapterTitle": [
       "Chapter 1"
      ]
     },
     {
      "paragraph": [
       "This is my story."
      ]
     },
     {
      "paragraph": [
       "I separate paragraphs with 2 newlines."
      ]
     }
    ]
   },
   {
    "chapter": [
     {
      "chapterTitle": [
       "Chapter 2"
      ]
     },
     {
      "paragraph": [
       "The end!"
      ]
     }
    ]
   }
  ]
 }
]
*/

```

### Collections
Collections are simply references to other hyperpubees:

```
const ram = require('random-access-memory')
const HyperInterface = require('hyperpubee-hyper-interface')
const { api, helpers } = require('hyperpubee')
const Corestore = require('corestore')

const corestore = new Corestore(ram)
const hyperInterface = new HyperInterface(corestore)
await hyperInterface.ready()

// Creating a collection

// Note: you should use hashes of existing and available works.
// These dummy hashes serve only as illustration of the concept.
const hash1 = "a".repeat(64)
const hash2 = "b".repeat(64)

const text = `[collection]
# My Collection

[${hash1}]
[${hash2}/structure/chapter/2]
`

pubee = await api.buildWorkFromText(
  text,
  'my collection',
  hyperInterface
)

console.log("Created collection with key", pubee.key)

const resolvedPubee = await helpers.resolvePubeeToJson(pubee)
console.log(JSON.stringify(resolvedPubee, null, 1))

/* Expected output:
[
 {
  "title": [
   "My Collection"
  ]
 },
 {
  "collection": [
   {
    "linkedHash": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
   },
   {
    "linkedHash": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
    "linkedLocation": "structure\u0000chapter\u000002"
   }
  ]
 }
]
*/
```
