const { parseWork } = require('./creation/parser')
const { Pubee } = require('./core/pubee')
const { getRootKey } = require('./core/utils')
const { Embedding } = require('./core/embedding')
const c = require('./core/constants')

async function readWork ({ hash, hyperInterface, version = undefined }) {
  const bee = await hyperInterface.readHyperbee(hash)
  const snapshot = version == null ? bee.snapshot() : bee.checkout(version)

  const pubee = new Pubee(snapshot)
  return pubee
}

async function buildWorkFromText ({ text, name, hyperInterface, author = undefined }) {
  const bee = await hyperInterface.createHyperbee(name)
  const pubee = await parseWork({ text, bee, author })

  return pubee
}

async function getEntry ({ hash, location, hyperInterface, version = undefined }) {
  const pubee = await readWork({ hash, hyperInterface, version })
  await pubee.ensureIsReadable()

  const res = await pubee.bee.get(location)
  return res?.value
}

async function getRoot ({ hash, hyperInterface, version = undefined }) {
  return await getEntry({ hash, location: getRootKey(), hyperInterface, version })
}

async function getAllEmbeddings ({ hash, hyperInterface, version = undefined }) {
  const pubee = await readWork({ hash, hyperInterface, version })
  await pubee.ensureIsReadable()

  const stream = pubee.bee.sub(c.EMBEDDING).createReadStream()

  const res = []
  // DEVNOTE: consider passing the stream directly and let the
  // caller worry about whether to preload all or handle as they come in
  // (avoids blocking on all embeddings if one is unavailable)
  for await (const { value: rawEmbedding } of stream) {
    res.push(new Embedding(rawEmbedding))
  }

  return res
}

module.exports = {
  readWork,
  buildWorkFromText,
  getEntry,
  getRoot,
  getAllEmbeddings
}
