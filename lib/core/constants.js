const b4a = require('b4a')

const EMPTY = b4a.toString(b4a.alloc(1))

const HYPERHASH_REGEX_STR = '[0-9a-fA-F]{64,64}'
const HYPERHASH_REGEX = new RegExp(`^${HYPERHASH_REGEX_STR}$`)

const KEYNAME_REGEX_STR = '\\w{1,64}'
const KEYNAME_REGEX = new RegExp(`^${KEYNAME_REGEX_STR}$`)

const hexRegexStr = '[0-9a-fA-F]'
const PUBEE_LOCATION_REGEX = new RegExp(String.raw`(${KEYNAME_REGEX_STR}${EMPTY})+${hexRegexStr}+`)

module.exports = {
  CONTENT: 'content',
  TITLE: 'title',
  STRUCTURE: 'structure',
  ROOT: 'root',
  // Careful if ever changing this to another separator
  // Some implicit dependencies (e.g. in pubee-validator)
  EMPTY,
  POEM: 'poem',
  LINE: 'line',
  VERSE: 'verse',
  EMBEDDING: 'embedding',
  EMBEDDING_HASH: 'referencedHash',
  EMBEDDING_LOCATION: 'referencedLocation',
  EMBEDDING_PATCH: 'patch',
  LINK: 'link',
  LINK_HASH: 'linkedHash',
  LINK_LOCATION: 'linkedLocation',
  HYPERHASH_REGEX,
  HYPERHASH_REGEX_STR,
  PUBEE_LOCATION_REGEX,
  CHAPTER: 'chapter',
  CHAPTER_TITLE: 'chapterTitle',
  KEYNAME_REGEX_STR,
  KEYNAME_REGEX,
  PROSE: 'prose',
  PARAGRAPH: 'paragraph',
  METADATA: 'metadata',
  METADATA_AUTHOR: 'author',
  COLLECTION: 'collection'
}
