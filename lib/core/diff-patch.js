/* eslint-disable camelcase */
/* eslint-disable new-cap */
const { diff_match_patch } = require('diff-match-patch')
require('diff-match-patch-line-and-word') // import globally to  enhance the class.

const dmp = new diff_match_patch()

function calcDiff (original, target) {
  const diffs = dmp.diff_wordMode(original, target)
  return diffs
}

function applyDiffToText (diff, text) {
  const patches = dmp.patch_make(text, diff)
  return dmp.patch_apply(patches, text)[0]
}
/* eslint-enable camelcase */
/* eslint-enable new-cap */

module.exports = {
  calcDiff,
  applyDiffToText
}
