const lexi = require('lexicographic-integer')
const { NotAnIntegerError, NotUnpackableError } = require('../exceptions')

function pack (intNr) {
  if (!Number.isInteger(intNr)) {
    throw new NotAnIntegerError(`'${intNr}' is not a valid integer`)
  }
  return lexi.pack(intNr, 'hex')
}

function unpack (txtNr) {
  const res = lexi.unpack(txtNr, 'hex')
  if (res === undefined) {
    throw new NotUnpackableError(`'${txtNr}' is not unpackable to an integer`)
  }
  return res
}

function * generator (startI = 0) {
  let i = startI
  while (true) {
    yield pack(i++)
  }
}

module.exports = {
  pack,
  unpack,
  generator
}
