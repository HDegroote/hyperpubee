const { isValidHyperHash, isValidPubeeLocation } = require('./utils')
const { ValidationError } = require('../exceptions')

class Link {
  constructor ({ linkedHash, linkedLocation }) {
    if (!isValidHyperHash(linkedHash)) {
      throw new ValidationError(
        `LinkedHash must be a valid hypercore hash (${linkedHash})`
      )
    }

    if (linkedLocation !== undefined) {
      if (!isValidPubeeLocation(linkedLocation)) {
        throw new ValidationError(
          `Invalid linked location (${linkedLocation})`
        )
      }
    }

    this.linkedHash = linkedHash
    this.linkedLocation = linkedLocation
  }
}

function validateLink (link) {
  // Creating a new link runs all validation tests
  new Link(link) // eslint-disable-line no-new
}

module.exports = {
  Link,
  validateLink
}
