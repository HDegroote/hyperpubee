const { once } = require('events')
const { STRUCTURE, ROOT } = require('./constants')
const { ensureIsValidPubee } = require('./pubee-validator')
const { getKeyAsStr } = require('./utils')
const { InvalidKeyError } = require('../exceptions')

class Pubee {
  constructor (bee) {
    this.bee = bee
  }

  async ensureIsValid () {
    await ensureIsValidPubee(this.bee)
  }

  isWritable () {
    return this.bee.feed.writable
  }

  async ensureIsReadable () {
    if (this.bee.feed.writable || this.bee.feed.peers.length) {
      return true
    }

    // TODO:? add timeout
    console.log('Waiting for peers to connect')
    const [peer] = await once(this.bee.feed, 'peer-add')
    console.log('Connected to peer', peer.remotePublicKey)

    return true
  }

  getReadOnlyBee () {
    return this.bee.snapshot()
  }

  async getChildrenOfRoot () {
    const structBee = this.bee.sub(STRUCTURE)

    const rootEntry = await structBee.get(ROOT)
    if (rootEntry === null) throw new InvalidKeyError('Root entry not found')

    return rootEntry.value
  }

  async get (key) {
    const entry = await this.bee.get(key)

    if (entry === null) {
      throw new InvalidKeyError(`No entry found for key '${key}'`)
    }

    return entry.value
  }

  get key () {
    return getKeyAsStr(this.bee.feed.key)
  }
}

module.exports = {
  Pubee
}
