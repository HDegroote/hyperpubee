const {
  POEM,
  ROOT,
  TITLE,
  LINE,
  VERSE,
  CHAPTER,
  CHAPTER_TITLE,
  PROSE,
  PARAGRAPH,
  COLLECTION
} = require('../core/constants')
const { Embedding } = require('../core/embedding')
const { NotImplementedError } = require('../exceptions')
const { Link } = require('../core/link')

class PartCreator {
  addToPubeeBuilder (/* pubeeBuilder */) {
    throw new NotImplementedError('addToPubeeBuilder has not been implemented')
  }
}

class ContentCreator extends PartCreator {
  content

  constructor (content) {
    super()
    this.content = content
  }

  async addToPubeeBuilder (pubeeBuilder) {
    const contentKey = await pubeeBuilder.addContent(this.content)
    return contentKey
  }
}

class StructureCreator extends PartCreator {
  constructor (structureName, childParts) {
    super()

    this.structureName = structureName
    this.childParts = childParts.slice()
  }

  async addToPubeeBuilder (pubeeBuilder) {
    const childKeys = await this._buildChildPartsAndGetTheirKeys(pubeeBuilder)
    const structureKey = await this._addStructure(childKeys, pubeeBuilder)

    return structureKey
  }

  async _buildChildPartsAndGetTheirKeys (pubeeBuilder) {
    const childKeyPromises = []

    for (const child of this.childParts) {
      const keyPromise = child.addToPubeeBuilder(pubeeBuilder)
      childKeyPromises.push(keyPromise)
    }

    const childKeys = await Promise.all(childKeyPromises)
    return childKeys
  }

  async _addStructure (childKeys, pubeeBuilder) {
    const structureKey = await pubeeBuilder.addStructure(
      this.structureName,
      childKeys
    )

    return structureKey
  }
}

class LineCreator extends StructureCreator {
  constructor (childParts) {
    super(LINE, childParts)
  }
}

class PoemCreator extends StructureCreator {
  constructor (childParts) {
    super(POEM, childParts)
  }
}

class TitleCreator extends StructureCreator {
  constructor (childParts) {
    super(TITLE, childParts)
  }
}

class VerseCreator extends StructureCreator {
  constructor (childParts) {
    super(VERSE, childParts)
  }
}

class ChapterCreator extends StructureCreator {
  constructor (childParts) {
    super(CHAPTER, childParts)
  }
}

class ChapterTitleCreator extends StructureCreator {
  constructor (childParts) {
    super(CHAPTER_TITLE, childParts)
  }
}

class ParagraphCreator extends StructureCreator {
  constructor (childParts) {
    super(PARAGRAPH, childParts)
  }
}

class ProseCreator extends StructureCreator {
  constructor (childParts) {
    super(PROSE, childParts)
  }
}

class CollectionCreator extends StructureCreator {
  constructor (childParts) {
    super(COLLECTION, childParts)
  }
}

class WorkCreator extends StructureCreator {
  constructor (childParts) {
    super(ROOT, childParts)
  }

  // Override
  async _addStructure (childKeys, pubeeBuilder) {
    const structureKey = await pubeeBuilder.addRoot(childKeys)
    return structureKey
  }
}

class EmbeddingCreator extends PartCreator {
  constructor (embedding) {
    super()
    this.embedding = new Embedding(embedding)
  }

  async addToPubeeBuilder (pubeeBuilder) {
    const key = await pubeeBuilder.addEmbedding(this.embedding)
    return key
  }
}

class LinkCreator extends PartCreator {
  constructor (link) {
    super()
    this.link = new Link(link)
  }

  async addToPubeeBuilder (pubeeBuilder) {
    const key = await pubeeBuilder.addLink(this.link)
    return key
  }
}

module.exports = {
  ContentCreator,
  StructureCreator,
  PoemCreator,
  WorkCreator,
  LineCreator,
  TitleCreator,
  VerseCreator,
  EmbeddingCreator,
  ChapterCreator,
  ChapterTitleCreator,
  ParagraphCreator,
  ProseCreator,
  LinkCreator,
  CollectionCreator
}
