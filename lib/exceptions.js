class EmbeddingValidationError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

class NotAnIntegerError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

class NotUnpackableError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

class ValidationError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

class NotImplementedError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

class InvalidKeyError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

module.exports = {
  EmbeddingValidationError,
  NotAnIntegerError,
  NotUnpackableError,
  ValidationError,
  NotImplementedError,
  InvalidKeyError
}
