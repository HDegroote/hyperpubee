const c = require('./core/constants')
const {
  isKeyForContent,
  isKeyForStructure,
  isKeyForEmbedding,
  isKeyForLink,
  getSubstructureNameFromKey
} = require('./core/utils')

function embeddingToJson (embedding) {
  return {
    [c.EMBEDDING_HASH]: embedding.referencedHash,
    [c.EMBEDDING_LOCATION]: embedding.referencedLocation,
    [c.EMBEDDING_PATCH]: embedding.patch
  }
}

function linkToJson (link) {
  const res = { [c.LINK_HASH]: link.linkedHash }

  const location = link.linkedLocation
  if (location !== undefined) {
    res[c.LINK_LOCATION] = location
  }

  return res
}

async function resolvePubeeChildToJson (child, pubee) {
  if (isKeyForContent(child)) {
    return await pubee.get(child)
  } else if (isKeyForStructure(child)) {
    const subChildren = []

    const children = await pubee.get(child)
    for (const subChild of children) {
      const subChildJson = await resolvePubeeChildToJson(subChild, pubee)
      subChildren.push(subChildJson)
    }

    const structType = getSubstructureNameFromKey(child)
    return { [structType]: subChildren }
  } else if (isKeyForEmbedding(child)) {
    const embedding = await pubee.get(child)
    return embeddingToJson(embedding)
  } else if (isKeyForLink(child)) {
    const link = await pubee.get(child)
    return linkToJson(link)
  } else {
    throw Error('Some invalid or not-yet-implemented type')
  }
}

async function resolvePubeeToJson (pubee) {
  // Returns a JSON starting from level below root,
  // where contents are rendered as text
  // (referenced contents are resolved to their plaintext)
  const rootChildren = await pubee.getChildrenOfRoot()

  const res = []
  for (const child of rootChildren) {
    const subJson = await resolvePubeeChildToJson(child, pubee)
    res.push(subJson)
  }

  return res
}

async function getTitleAsText (pubee) {
  const workJson = await resolvePubeeToJson(pubee)

  const titleComponent = workJson.find(
    (component) => component[c.TITLE] !== undefined
  )

  const title =
    titleComponent !== undefined ? titleComponent[c.TITLE].join(' ') : ''

  return title
}

module.exports = { getTitleAsText, resolvePubeeToJson }
