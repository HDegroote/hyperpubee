const Corestore = require('corestore')
const ram = require('random-access-memory')

const HyperInterface = require('hyperpubee-hyper-interface')

async function hyperInterfaceFactory () {
  const corestore = new Corestore(ram)

  const hyperInterface = new HyperInterface(corestore)
  await hyperInterface.ready()

  return hyperInterface
}

module.exports = {
  hyperInterfaceFactory
}
