const path = require('path')
const { promises: fs } = require('fs')

const { parseWork } = require('../lib/creation/parser')

const DATA_DIR = path.resolve(path.join('test', 'data'))

function getLines () {
  return ['Line 1', 'Line 2']
}

function getTitle () {
  return 'Example poem'
}

async function _getBee (hyperInterface) {
  // To avoid name clashes, we generate a random name for each bee
  const randomName = Math.random().toString()
  const bee = await hyperInterface.createHyperbee(randomName)

  return bee
}

async function getPubee (hyperInterface) {
  const poemText =
    ['[poetry]#', getTitle()].join(' ') + `\n\n${getLines().join('\n')}\n`

  const bee = await _getBee(hyperInterface)
  const pubee = await parseWork({ text: poemText, bee })

  return pubee
}

async function getPubeeWithoutTitle (hyperInterface) {
  const poemText = `[poetry]${getLines().join('\n')}\n`

  const bee = await _getBee(hyperInterface)
  const pubee = await parseWork({ text: poemText, bee })

  return pubee
}

async function readFileFromDataDir (name) {
  const location = path.join(DATA_DIR, name)
  const content = await fs.readFile(location, 'utf-8')
  return content
}

async function getSimplePoemTxt () {
  return await readFileFromDataDir('simplePoem.txt')
}

async function getChapteredPoemTxt () {
  return await readFileFromDataDir('chapteredPoem.txt')
}

async function getChapteredProseTxt () {
  return await readFileFromDataDir('chapteredProse.txt')
}

async function getInvalidChapteredProseTxt () {
  return await readFileFromDataDir('invalidChapteredProse.txt')
}

module.exports = {
  getLines,
  getPubee,
  getTitle,
  getSimplePoemTxt,
  getChapteredPoemTxt,
  getPubeeWithoutTitle,
  getChapteredProseTxt,
  getInvalidChapteredProseTxt
}
