const { expect } = require('chai')

const { createArrayFromStream } = require('../lib/core/utils')
const { CONTENT, METADATA, EMBEDDING } = require('../lib/core/constants')
const api = require('../lib/api')
const { hyperInterfaceFactory } = require('./fixtures')
const utils = require('../lib/core/utils')
const { Embedding } = require('../lib/core/embedding')

describe('Test api', function () {
  let hyperInterface

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  it('can build a poem from text', async function () {
    const text = '[poetry]# My poem title\n\nLine 1\nLine 2\nLine 3'

    const poemHyperbee = await api.buildWorkFromText({
      text,
      name: 'my work',
      hyperInterface
    })
    const bee = await poemHyperbee.getReadOnlyBee()

    const actualContent = await createArrayFromStream(
      bee.sub(CONTENT).createReadStream()
    )
    const expectedContent = ['My poem title', 'Line 1', 'Line 2', 'Line 3']
    expect(actualContent).to.deep.equal(expectedContent)
  })

  it('can build prose from text', async function () {
    const text = '[prose]# My prose title\n\nparagraph1.\n\nParagraph2.'

    const pubee = await api.buildWorkFromText({
      text,
      name: 'my work',
      hyperInterface
    })
    const bee = await pubee.getReadOnlyBee()

    const actualContent = await createArrayFromStream(
      bee.sub(CONTENT).createReadStream()
    )
    const expectedContent = ['My prose title', 'paragraph1.', 'Paragraph2.']
    expect(actualContent).to.deep.equal(expectedContent)
  })

  it('can add an author', async function () {
    const text = '[prose]paragraph1.'

    const pubee = await api.buildWorkFromText({
      text,
      name: 'my work',
      hyperInterface,
      author: 'Someone'
    })
    const metadata = await pubee.get(METADATA)
    expect(metadata.author).to.equal('Someone')
  })

  describe('Test reading a pubee', function () {
    let originalBee
    const location = utils.createHexLexedReference(CONTENT, 0)

    this.beforeEach(async function () {
      const text = '[poetry]# The title\n\nLine 1\nLine 2'
      originalBee = await api.buildWorkFromText({
        text,
        name: 'my test work',
        hyperInterface
      })
    })

    it('Can read a pubee from key', async function () {
      const otherBee = await api.readWork(
        { hash: originalBee.key, hyperInterface }
      )
      await otherBee.ensureIsReadable()

      const readContentStream = otherBee.bee.createReadStream()
      const readContent = await createArrayFromStream(readContentStream)

      const originalContentStream = originalBee.bee.createReadStream()
      const originalContent = await createArrayFromStream(
        originalContentStream
      )

      expect(originalContent.length).to.equal(10) // Sanity check
      expect(originalContent).to.deep.equal(readContent)
    })

    describe('Test versioning logic', function () {
      let initV, newV

      this.beforeEach(async function () {
        initV = originalBee.bee.version
        await originalBee.bee.put(location, 'new text')
        newV = originalBee.bee.version
      })

      it('Can read old pubee version from key', async function () {
        const otherBeeLatest = await api.readWork(
          { hash: originalBee.key, hyperInterface }
        )
        await otherBeeLatest.ensureIsReadable()
        const readNewContent = await otherBeeLatest.get(location)

        const otherBeeOld = await api.readWork(
          { hash: originalBee.key, version: initV, hyperInterface }
        )
        await otherBeeOld.ensureIsReadable()
        const readOldContent = await otherBeeOld.get(location)

        expect(readNewContent).to.equal('new text')
        expect(otherBeeLatest.bee.version).to.equal(newV)

        expect(readOldContent).to.equal('The title')
        expect(otherBeeOld.bee.version).to.equal(initV)
      })

      it('Can get an old old entry', async function () {
        const newContent = await api.getEntry(
          { hash: originalBee.key, location, hyperInterface }
        )
        const oldContent = await api.getEntry(
          { hash: originalBee.key, location, hyperInterface, version: initV }
        )

        expect(newContent).to.equal('new text')
        expect(oldContent).to.equal('The title') // Sanity check
      })

      it('Can get an old root', async function () {
        const newRootTxt = 'invalid pubee, but easy test'
        await originalBee.bee.put(utils.getRootKey(), newRootTxt)

        const newRoot = await api.getRoot(
          { hash: originalBee.key, hyperInterface }
        )
        const oldRoot = await api.getRoot(
          { hash: originalBee.key, hyperInterface, version: initV }
        )

        expect(newRoot).to.equal(newRootTxt)
        expect(oldRoot).to.not.equal(newRootTxt) // Sanity check
      })

      it('Can get an old embedding', async function () {
        const newEmbedding = new Embedding(
          { referencedHash: 'a'.repeat(64), referencedLocation: location, patch: [] }
        )
        const embeddinglocation = utils.createHexLexedReference(EMBEDDING, 0)
        await originalBee.bee.put(embeddinglocation, newEmbedding)

        const newEmbeddings = await api.getAllEmbeddings({
          hash: originalBee.key,
          location: embeddinglocation,
          hyperInterface
        })

        const oldEmbeddings = await api.getAllEmbeddings({
          hash: originalBee.key,
          location: embeddinglocation,
          hyperInterface,
          version: initV
        })

        expect(newEmbeddings).to.deep.equal([newEmbedding])
        expect(oldEmbeddings).to.deep.equal([]) // Sanity check
      })
    })

    it('Can get the root', async function () {
      const root = await api.getRoot(
        { hash: originalBee.key, hyperInterface }
      )
      expect(root.length).to.equal(2) // Title and poem
    })

    it('Can get an entry', async function () {
      const content = await api.getEntry(
        { hash: originalBee.key, location, hyperInterface }
      )
      expect(content).to.equal('The title')
    })

    it('Can get all embeddings', async function () {
      const text = `[poetry]
        Embed <${'a'.repeat(64)}/content/0>
        More <${'b'.repeat(64)}/content/4>
      `
      const pubee = await api.buildWorkFromText({
        text,
        name: 'test work',
        hyperInterface
      })

      const embeddings = await api.getAllEmbeddings(
        { hash: pubee.key, hyperInterface }
      )

      expect(embeddings.length).to.deep.equal(2)
      expect(embeddings[0].referencedHash === 'a'.repeat(64))
      expect(embeddings[1].referencedHash === 'b'.repeat(64))
    })

    // TODO: Missings tests: what happens if key not found?
    // Invalid key? Valid key but no hyperbee/hyperpubee?
  })
})
