const { expect } = require('chai')

const { calcDiff, applyDiffToText } = require('../lib/core/diff-patch')

describe('Test diffpatch', function () {
  let original
  let transformed
  let expectedPatches

  this.beforeEach(function () {
    original = 'I am different'
    transformed = "I'm differents"
    expectedPatches = [
      [0, 'I'],
      [-1, ' am'],
      [1, "'m"],
      [0, ' '],
      [-1, 'different'],
      [1, 'differents']
    ]
  })

  it('sees no diff for identicals', function () {
    const diffs = calcDiff('I am the same', 'I am the same')
    const expected = [[0, 'I am the same']]

    expect(diffs).to.deep.equal(expected)
  })

  it('Makes word-level diffs for non-identicals', function () {
    expect(calcDiff(original, transformed)).to.deep.equal(expectedPatches)
  })

  it('can restore original from patches and transformed', function () {
    const patches = calcDiff(original, transformed)
    expect(patches).to.deep.equal(expectedPatches) // Sanity check

    const patchedOriginal = applyDiffToText(patches, original)

    expect(patchedOriginal).to.deep.equal(transformed)
  })
})
