const { expect } = require('chai')

const { CONTENT } = require('../lib/core/constants')
const { getAbsoluteKey } = require('../lib/core/utils')
const hexlexi = require('../lib/core/hexlexi')
const { validateEmbedding, Embedding } = require('../lib/core/embedding')

describe('Embedding tests', function () {
  let index0
  let embeddingJson

  this.beforeEach(async function () {
    index0 = hexlexi.pack(0)
    embeddingJson = {
      referencedHash: 'a'.repeat(64),
      referencedLocation: getAbsoluteKey(CONTENT, index0),
      patch: []
    }
  })

  it('validates a valid embedding JSON', function () {
    validateEmbedding(embeddingJson)
    // Nothing thrown means valid
  })

  it('validates a valid embedding object', function () {
    const embedding = new Embedding(embeddingJson)
    validateEmbedding(embedding)
    // Nothing thrown means valid
  })

  it('Throws on missing referencedHash', function () {
    delete embeddingJson.referencedHash
    expect(() => validateEmbedding(embeddingJson)).to.throw(
      'referencedHash must be a valid hypercore hash (undefined)'
    )
  })

  it('Throws on missing referencedLocation', function () {
    delete embeddingJson.referencedLocation
    expect(() => validateEmbedding(embeddingJson)).to.throw(
      'Invalid referenced location (undefined)'
    )
  })

  it('Throws on invalid referencedHash', function () {
    embeddingJson.referencedHash = 'notAKey'
    expect(() => validateEmbedding(embeddingJson)).to.throw(
      'referencedHash must be a valid hypercore hash'
    )
  })

  it('correctly applies a patch', function () {
    embeddingJson.patch = [[-1, 'Goo'], [1, 'Ba'], [0, 'd dog']]
    const embedding = new Embedding(embeddingJson)

    const expected = 'Bad dog'
    expect(embedding.applyPatch('Good dog')).to.equal(expected)
  })

  describe('Test invalids for patch validation', function () {
    const invalidPatches = [
      [4, 'should be -1, 0 or 1'],
      [0, 911],
      [0, 'ok', 'ko'],
      [],
      [0]
    ]

    invalidPatches.forEach((patch) => {
      const strPatch = JSON.stringify(patch)

      it(`throws on invalid patch ${strPatch}`, function () {
        embeddingJson.patch = [patch]
        expect(() => validateEmbedding(embeddingJson)).to.throw(
          `Invalid patch structure for element '${strPatch}'`
        )
      })
    })
  })

  it('Throws on missing patch', function () {
    delete embeddingJson.patch
    expect(() => validateEmbedding(embeddingJson)).to.throw(
      'patch must be an array (use empty array if none to apply)'
    )
  })
})
