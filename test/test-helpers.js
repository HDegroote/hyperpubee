const { expect } = require('chai')
const ram = require('random-access-memory')
const Corestore = require('corestore')

const HyperInterface = require('hyperpubee-hyper-interface')
const { resolvePubeeToJson, getTitleAsText } = require('../lib/helpers')
const c = require('../lib/core/constants')
const { getPubee, getTitle, getLines, getPubeeWithoutTitle } = require('./shared-data')

describe('Test helpers', async function () {
  let hyperInterface
  let corestore

  this.beforeEach(async function () {
    corestore = new Corestore(ram)
    await corestore.ready()
    hyperInterface = new HyperInterface(corestore)
  })

  this.afterEach(async function () {
    await corestore.close()
  })

  it('resolvePubeeToJson can resolve a simple pubee', async function () {
    const pubee = await getPubee(hyperInterface)

    const actual = await resolvePubeeToJson(pubee)
    const lines = getLines()
    const expected = [
      { [c.TITLE]: [getTitle()] },
      {
        [c.POEM]: [
          { [c.VERSE]: [{ [c.LINE]: [lines[0]] }, { [c.LINE]: [lines[1]] }] }
        ]
      }
    ]
    expect(actual).to.deep.equal(expected)
  })

  it('getTitleAsText returns correct title', async function () {
    const pubee = await getPubee(hyperInterface)
    expect((await getTitleAsText(pubee))).to.equal(getTitle())
  })

  it('getTitleAsText returns empty string if no title', async function () {
    const pubee = await getPubeeWithoutTitle(hyperInterface)
    expect((await getTitleAsText(pubee))).to.equal('')
  })
})
