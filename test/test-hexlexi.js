const { strict: nodeAssert } = require('assert')
const { expect } = require('chai')

const hexlexi = require('../lib/core/hexlexi')

describe('Test Lexi', function () {
  const tests = [
    { nr: 0, txt: '00' },
    { nr: 1, txt: '01' },
    { nr: 256, txt: 'fb05' },
    { nr: 5004, txt: 'fc1291' }
  ]
  tests.forEach(({ nr, txt }) => {
    it(`Can pack an integer ${nr}->${txt}`, function () {
      expect(hexlexi.pack(nr)).to.eq(txt)
    })
  })

  tests.forEach(({ txt, nr }) => {
    it(`Can unpack a hex str ${txt}->${nr}`, function () {
      expect(hexlexi.unpack(txt)).to.eq(nr)
    })
  })

  it('Can generate the first 4 values', function () {
    const generator = hexlexi.generator()
    const expected = ['00', '01', '02', '03']
    const values = []
    for (let i = 0; i < 4; i++) {
      values.push(generator.next().value)
    }
    expect(values).to.deep.eq(expected)
  })

  it('Can generate the first 4 values starting from 5', function () {
    const generator = hexlexi.generator(5)
    const expected = ['05', '06', '07', '08']
    const values = []
    for (let i = 0; i < 4; i++) {
      values.push(generator.next().value)
    }
    expect(values).to.deep.eq(expected)
  })

  it('can generate arbitrary many values', function () {
    const generator = hexlexi.generator()
    let lastValue = ''
    for (let i = 0; i <= 5004; i++) {
      lastValue = generator.next().value
    }
    expect(lastValue).to.eq('fc1291')
  })

  it('Throws a NotAnIntegerError when packing a non-integer', function () {
    nodeAssert.throws(() => hexlexi.pack('nope'), {
      message: "'nope' is not a valid integer"
    })
  })

  it('Throws a NotAnIntegerError when unpacking something invalid', function () {
    nodeAssert.throws(() => hexlexi.unpack('nope'), {
      message: "'nope' is not unpackable to an integer"
    })
  })
})
