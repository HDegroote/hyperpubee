const { expect } = require('chai')

const index = require('../index')

describe('Test exported interface', function () {
  it('Index contains the expected keys', function () {
    const expectedKeys = [
      'api',
      'exceptions',
      'constants',
      'Embedding',
      'ensureIsValidPubee',
      'Pubee',
      'utils',
      'parsePoem',
      'helpers'
    ].sort()
    expect(Object.keys(index).sort()).to.deep.equal(expectedKeys)
  })
})
