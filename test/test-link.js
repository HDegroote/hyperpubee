const { expect } = require('chai')

const { CONTENT } = require('../lib/core/constants')
const { getAbsoluteKey } = require('../lib/core/utils')
const hexlexi = require('../lib/core/hexlexi')
const { Link, validateLink } = require('../lib/core/link')

describe('Link tests', function () {
  let index0
  let linkJson

  this.beforeEach(async function () {
    index0 = hexlexi.pack(0)
    linkJson = {
      linkedHash: 'a'.repeat(64)
    }
  })

  it('Can create a link without location', function () {
    const link = new Link(linkJson)

    expect(link.linkedHash).to.equal('a'.repeat(64))
    expect(link.linkedLocation).to.equal(undefined)
  })

  it('Can create a link with location', function () {
    const location = getAbsoluteKey(CONTENT, index0)
    linkJson.linkedLocation = location
    const link = new Link(linkJson)

    expect(link.linkedHash).to.equal('a'.repeat(64))
    expect(link.linkedLocation).to.equal(location)
  })

  it('Does not validate a Link with non-str location', function () {
    const location = ['Nope']
    linkJson.linkedLocation = location

    expect(() => validateLink(linkJson)).to.throw(
      'Invalid linked location (Nope)'
    )
  })

  it('Does not validate a Link with invalid hash', function () {
    linkJson.linkedHash = 'aaa'

    expect(() => validateLink(linkJson)).to.throw(
      'LinkedHash must be a valid hypercore hash'
    )
  })
})
