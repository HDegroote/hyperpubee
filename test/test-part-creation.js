const sinon = require('sinon')
const { expect, assert } = require('chai')

const { hyperInterfaceFactory } = require('./fixtures')
const PubeeBuilder = require('../lib/creation/pubee-builder')
const hexlexi = require('../lib/core/hexlexi')
const {
  ContentCreator,
  StructureCreator,
  PoemCreator,
  WorkCreator,
  LineCreator,
  TitleCreator,
  VerseCreator,
  EmbeddingCreator,
  ChapterCreator,
  ChapterTitleCreator,
  ParagraphCreator,
  ProseCreator,
  LinkCreator,
  CollectionCreator
} = require('../lib/creation/part-creation')
const {
  POEM,
  LINE,
  TITLE,
  VERSE,
  CONTENT,
  CHAPTER,
  CHAPTER_TITLE,
  PROSE,
  PARAGRAPH,
  COLLECTION
} = require('../lib/core/constants')
const { Embedding } = require('../lib/core/embedding')
const { getAbsoluteKey } = require('../lib/core/utils')
const { Link } = require('../lib/core/link')

describe('Test pubeePartCreation', function () {
  let hyperInterface
  let builder
  let contentCreator
  const index0 = hexlexi.pack(0)

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()

    const hyperbee = await hyperInterface.createHyperbee('testbee')
    builder = new PubeeBuilder(hyperbee)

    contentCreator = new ContentCreator('My content')
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  it('can add content to a pubeeBuilder', async function () {
    const buildSpy = sinon.spy(builder, 'addContent')

    await contentCreator.addToPubeeBuilder(builder)
    assert(buildSpy.calledOnce)
    expect(buildSpy.firstCall.args).deep.equals(['My content'])
  })

  it('can add a structure to a pubeeBuilder', async function () {
    const addStructureSpy = sinon.spy(builder, 'addStructure')
    const addContentSpy = sinon.spy(builder, 'addContent')

    const structCreator = new StructureCreator('myStruct', [contentCreator])

    await structCreator.addToPubeeBuilder(builder)
    assert(addContentSpy.calledOnce)
    expect(addContentSpy.firstCall.args).deep.equals(['My content'])

    assert(addStructureSpy.calledOnce)
    // called with the expected child (the key of the content)
    const contentKey = await addContentSpy.firstCall.returnValue
    expect(addStructureSpy.firstCall.args).deep.equals([
      'myStruct',
      [contentKey]
    ])
  })

  describe('Testsing the initialisation of simple structures', function () {
    const testCases = [
      { structureName: LINE, structureClass: LineCreator },
      { structureName: POEM, structureClass: PoemCreator },
      { structureName: TITLE, structureClass: TitleCreator },
      { structureName: VERSE, structureClass: VerseCreator },
      { structureName: CHAPTER, structureClass: ChapterCreator },
      { structureName: CHAPTER_TITLE, structureClass: ChapterTitleCreator },
      { structureName: PARAGRAPH, structureClass: ParagraphCreator },
      { structureName: PROSE, structureClass: ProseCreator },
      { structureName: COLLECTION, structureClass: CollectionCreator }

    ]

    testCases.forEach(({ structureName, structureClass }) => {
      it(`correctly inits ${structureClass.name}`, function () {
        const res = new structureClass(['mockChild']) // eslint-disable-line new-cap
        expect(res.structureName).to.equal(structureName)
      })
    })
  })

  it('correctly overrides the addStructure method for WorkCreator', async function () {
    const workCreator = new WorkCreator([contentCreator])
    const addRootSpy = sinon.spy(builder, 'addRoot')
    const addContentSpy = sinon.spy(builder, 'addContent')

    await workCreator.addToPubeeBuilder(builder)

    const contentKey = await addContentSpy.firstCall.returnValue
    assert(addRootSpy.calledOnce)
    expect(addRootSpy.firstCall.args).deep.equals([[contentKey]])
  })

  it('Can add an embedding to a pubeebuilder', async function () {
    const buildSpy = sinon.spy(builder, 'addEmbedding')
    const embedding = new Embedding({
      referencedHash: 'a'.repeat(64),
      referencedLocation: getAbsoluteKey(CONTENT, index0),
      patch: []
    })

    const embeddingCreator = new EmbeddingCreator(embedding)

    await embeddingCreator.addToPubeeBuilder(builder)
    assert(buildSpy.calledOnce)
    expect(buildSpy.firstCall.args.length).to.equal(1)

    const expected = Object.values(embedding)
    const actual = Object.values(buildSpy.firstCall.args[0])
    expect(actual).to.deep.equal(expected)
  })

  it('Can add a link to a pubeebuilder', async function () {
    const buildSpy = sinon.spy(builder, 'addLink')

    const link = new Link({
      linkedHash: 'a'.repeat(64),
      linkedLocation: getAbsoluteKey(CONTENT, index0)
    })

    const linkCreator = new LinkCreator(link)

    await linkCreator.addToPubeeBuilder(builder)
    assert(buildSpy.calledOnce)

    const expected = Object.values(link)
    const actual = Object.values(buildSpy.firstCall.args[0])
    expect(actual).to.deep.equal(expected)
  })
})
