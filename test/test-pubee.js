const { strict: nodeAssert } = require('assert')
const { expect } = require('chai')

const { POEM, TITLE, STRUCTURE, CONTENT } = require('../lib/core/constants')
const { Pubee } = require('../lib/core/pubee')
const { getAbsoluteKey, getKeyAsStr } = require('../lib/core/utils')
const hexlexi = require('../lib/core/hexlexi')
const { getLines, getPubee } = require('./shared-data')
const { hyperInterfaceFactory } = require('./fixtures')
const { InvalidKeyError } = require('../lib/exceptions')

describe('Hyperpubee tests', function () {
  let hyperInterface
  let pubee
  let lines

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
    lines = getLines()
    pubee = await getPubee(hyperInterface)
  })

  this.afterEach(async function () {
    await hyperInterface.close()
  })

  it('ensureIsReadable instantly returns for a self-owned pubee', async function () {
    expect((await pubee.ensureIsReadable())).to.equal(true)
  })

  describe('isWritable works as expected', function () {
    it('Returns true if the bee was created on this sdk', function () {
      expect(pubee.isWritable()).to.equal(true)
    })

    it('Returns false if the bee was created on another sdk', async function () {
      const otherHyperInterface = await hyperInterfaceFactory()

      const otherBee = await otherHyperInterface.readHyperbee(pubee.key)
      const otherPubee = new Pubee(otherBee)

      expect(otherPubee.isWritable()).to.equal(false)

      await otherHyperInterface.close()
    })
  })

  describe('Test accessor functions', function () {
    it('Can get the children of the root', async function () {
      const children = await pubee.getChildrenOfRoot()
      expect(children.length).to.equal(2)
      const titleKey = getAbsoluteKey(STRUCTURE, TITLE, hexlexi.pack(0))
      const poemKey = getAbsoluteKey(STRUCTURE, POEM, hexlexi.pack(0))

      expect(children).to.contain(titleKey)
      expect(children).to.contain(poemKey)
    })

    it('Can get an entry', async function () {
      // Note: the first entry in content is the title
      const key = getAbsoluteKey(CONTENT, hexlexi.pack(1))
      const line1 = await pubee.get(key)

      expect(line1).to.equal(lines[0])
    })

    it('Can get the key', async function () {
      expect(pubee.key).to.equal(getKeyAsStr(pubee.bee.feed.key))
    })

    describe('Test getters throw errors if key not found', async function () {
      let emptyPubee

      this.beforeEach(async function () {
        const bee = await hyperInterface.createHyperbee('testBee')
        emptyPubee = new Pubee(bee)
      })

      it('Throws an invalidKeyError when trying to get children for non-existent root', async function () {
        await nodeAssert.rejects(emptyPubee.getChildrenOfRoot(), {
          name: InvalidKeyError.name,
          message: 'Root entry not found'
        })
      })

      it('Throws an invalidKeyError when trying to get non-existent entry', async function () {
        await nodeAssert.rejects(emptyPubee.get('NothingHere'), {
          name: InvalidKeyError.name,
          message: "No entry found for key 'NothingHere'"
        })
      })
    })
  })
})
