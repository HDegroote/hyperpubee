const { expect } = require('chai')
const { strict: nodeAssert } = require('assert')
const Corestore = require('corestore')
const ram = require('random-access-memory')
const HyperInterface = require('hyperpubee-hyper-interface')

const hexlexi = require('../lib/core/hexlexi')
const {
  getAbsoluteKey,
  createArrayFromStream,
  isKeyForContent,
  isKeyForStructure,
  getSubstructureNameFromKey,
  createHexLexedReference,
  isValidPubeeLocation
} = require('../lib/core/utils')
const { EMPTY, CONTENT, STRUCTURE, VERSE } = require('../lib/core/constants')

describe('Test utils', function () {
  describe('Test getAbsoluteKey', function () {
    it('Returns the correct key for 1-degree case', function () {
      const absKey = getAbsoluteKey('mySub', 'myKey')
      const expected = ['mySub', EMPTY, 'myKey'].join('')

      expect(absKey).to.equal(expected)
    })

    it('Returns the correct key for multi-degree case', function () {
      const absKey = getAbsoluteKey('mySub', 'myKey1', 'myKey2', 'myKey3')

      const expected = ['mySub', 'myKey1', 'myKey2', 'myKey3'].join(EMPTY)
      expect(absKey).to.equal(expected)
    })
  })

  describe('Test createArrayFromStream', function () {
    let hyperInterface
    let corestore

    this.beforeEach(async function () {
      corestore = new Corestore(ram)
      await corestore.ready()

      hyperInterface = new HyperInterface(corestore)
    })

    this.afterEach(async function () {
      await corestore.close()
    })

    it('Returns the correct array when parsing a stream', async function () {
      const bee = await hyperInterface.createHyperbee('bee')
      await bee.put('a', 0)
      await bee.put('b', 1)

      const beeArray = await createArrayFromStream(bee.createReadStream())
      expect(beeArray).to.deep.equal([0, 1])
    })
  })

  describe('Test is key for', function () {
    it('Returns false when key does not contain proper separation symbol', function () {
      expect(isKeyForContent(CONTENT)).to.equal(false)
    })

    it('Returns false when key is not of expected type', function () {
      const res = isKeyForContent([STRUCTURE, 'more'].join(EMPTY))
      expect(res).to.equal(false)
    })

    it('isKeyForContent returns true for valid content key', function () {
      expect(isKeyForContent([CONTENT, 'more'].join(EMPTY))).to.equal(true)
    })

    it('isKeyForSTRUCTURE returns true for valid content key', function () {
      expect(isKeyForStructure([STRUCTURE, 'more'].join(EMPTY))).to.equal(true)
    })
  })

  describe('Test getSubstructureNameFromKey', function () {
    it('Throws when not a valid structure key', function () {
      nodeAssert.throws(
        () => getSubstructureNameFromKey([CONTENT, EMPTY, 'Nope'].join('')),
        {
          message: "Key 'content\x00Nope is not valid for a structure"
        }
      )
    })

    it('getSubstructureNameFromKey happy flow', function () {
      expect(
        getSubstructureNameFromKey([STRUCTURE, 'myStruct', '01'].join(EMPTY))
      ).to.equal('myStruct')
    })
  })

  it('Can construct a reference ', function () {
    const actual = createHexLexedReference('content', 25)
    const expected = ['content', hexlexi.pack(25)].join(EMPTY)
    expect(actual).to.eq(expected)
  })

  describe('Test isValidPubeeLocation', function () {
    const nestedKey = getAbsoluteKey(STRUCTURE, VERSE)

    const testCases = [
      createHexLexedReference(CONTENT, 0),
      createHexLexedReference(CONTENT, 14),
      createHexLexedReference(CONTENT, 1045),
      createHexLexedReference(nestedKey, 0),
      createHexLexedReference(nestedKey, 14),
      createHexLexedReference(nestedKey, 1045)
    ]

    testCases.forEach((location) => {
      it(`Accepts valid location ${location}`, function () {
        expect(isValidPubeeLocation(location)).to.equal(true)
      })
    })
  })
})
